package com.kfwebstandard.jpaqueryworld.servlets;

import com.kfwebstandard.jpaqueryworld.entities.City;
import com.kfwebstandard.jpaqueryworld.entities.Country;
import com.kfwebstandard.jpaqueryworld.entities.CountryRegionLanguageBean;
import com.kfwebstandard.jpaqueryworld.entities.Country_;
import com.kfwebstandard.jpaqueryworld.entities.Countrylanguage;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Collection;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet that demonstrates a range of queries on the world database
 *
 * @author Ken Fogel
 */
@WebServlet(name = "JPAQueryDemoServlet", urlPatterns = {"/JPAQueryDemo"})
public class JPAQueryDemoServlet extends HttpServlet {

    private final static Logger LOG = LoggerFactory.getLogger(JPAQueryDemoServlet.class);
    
    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println(
                    "<!doctype html public \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n"
                    + "<html>\n"
                    + "<head>\n"
                    + "  <title>Country Records</title>\n"
                    + "</head>\n");

            // Basic Query
//            TypedQuery<Country> query = entityManager.createQuery("Select c from Country c", Country.class);
//            Collection<Country> countries = query.getResultList();
//            showTableCountry(countries, out);

            // Query with Join
//            TypedQuery<Country> query = entityManager.createQuery("SELECT c FROM Country c INNER JOIN c.countrylanguageCollection cl", Country.class);
//            Collection<Country> countries = query.getResultList();
//            showTableCountry(countries, out);
            
            // Query with Join and Where
//            TypedQuery<Country> query = entityManager.createQuery("SELECT c FROM Country c INNER JOIN c.countrylanguageCollection cl WHERE cl.countrylanguagePK.language = 'English'", Country.class);
//            Collection<Country> countries = query.getResultList();
//            showTableCountry(countries, out);
            
            // Query with Join and multi table Select
//            TypedQuery<Object[]> query= entityManager.createQuery("SELECT c.name, c.region, cl.countrylanguagePK.language FROM Country c INNER JOIN c.countrylanguageCollection cl",Object[].class);
//            Collection<Object[]> countries = query.getResultList();
//            showUnTypedTable(countries, out);
            
            // Query with Join and multi table Select and Query Class
//            TypedQuery<CountryRegionLanguageBean> query= 
//                    entityManager.createQuery("SELECT new com.kfwebstandard.jpaqueryworld.entities.CountryRegionLanguageBean(c1.name, c1.region, c2.countrylanguagePK.language) FROM Country c1 INNER JOIN c1.countrylanguageCollection c2 ORDER BY c2.countrylanguagePK.language",CountryRegionLanguageBean.class);
//            Collection<CountryRegionLanguageBean> countries = query.getResultList();
//            showMultiTableClass(countries, out);
            

            // Criteria for Select all records
//            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//            CriteriaQuery<Country> cq = cb.createQuery(Country.class);
//            Root<Country> country = cq.from(Country.class);
//            cq.select(country);
//            TypedQuery<Country> query = entityManager.createQuery(cq);
//            Collection<Country> countries = query.getResultList();
//            showTableCountry(countries, out);

            // Criteria for Select all records that begin with the letter C
//            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//            CriteriaQuery<Country> cq = cb.createQuery(Country.class);
//            Root<Country> country = cq.from(Country.class);
//            cq.select(country);
            // Use String to refernce a field
//            cq.where(cb.like(country.get("name"), "C%"));
//            cq.orderBy(cb.asc(country.get("name")));
            // Use metadata class to define the where clause
//            cq.where(cb.like(country.get(Country_.name), "C%"));
//            cq.orderBy(cb.asc(country.get(Country_.name)));
//
//            TypedQuery<Country> query = entityManager.createQuery(cq);
//            Collection<Country> countries = query.getResultList();
//            showTableCountry(countries, out);

            // Criteria with Join
//            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//            CriteriaQuery<Country> cq = cb.createQuery(Country.class);
//            Root<Country> country = cq.from(Country.class);
//            country.join("countrylanguageCollection");
//            TypedQuery<Country> query = entityManager.createQuery(cq);
//            Collection<Country> countries = query.getResultList();
//            showTableCountry(countries, out);

            // Criteria with Join and multi Select
//            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//            CriteriaQuery<Object[]> cq = cb.createQuery(Object[].class);
//            Root<Country> country = cq.from(Country.class);
//            Join countryLanguage = country.join("countrylanguageCollection");
//            cq.multiselect(country.get("name"), country.get("region"), countryLanguage.get("countrylanguagePK").get("language"));
//            TypedQuery<Object[]> query = entityManager.createQuery(cq);
//            Collection<Object[]> countries = query.getResultList();
//            showUnTypedTable(countries, out);

//          // Criteria with Join, Where and multi Select
//            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//            CriteriaQuery<Object[]> cq = cb.createQuery(Object[].class);
//            Root<Country> country = cq.from(Country.class);
//            Join countryLanguage = country.join("countrylanguageCollection");
//            cq.where(cb.equal(countryLanguage.get("countrylanguagePK").get("language"), "English"));
//            cq.multiselect(country.get("name"), country.get("region"), countryLanguage.get("countrylanguagePK").get("language"));
//            TypedQuery<Object[]> query = entityManager.createQuery(cq);
//            Collection<Object[]> countries = query.getResultList();
//            showUnTypedTable(countries, out);

            // Criteria with Join, Where and multi Select with Query class
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<CountryRegionLanguageBean> cq = cb.createQuery(CountryRegionLanguageBean.class);
            Root<Country> country = cq.from(Country.class);
            Join countryLanguage = country.join("countrylanguageCollection");
            cq.where(cb.equal(countryLanguage.get("countrylanguagePK").get("language"), "English"));
            cq.select(cb.construct(CountryRegionLanguageBean.class, country.get("name"), country.get("region"), countryLanguage.get("countrylanguagePK").get("language")));
            TypedQuery<CountryRegionLanguageBean> query = entityManager.createQuery(cq);
            Collection<CountryRegionLanguageBean> countries = query.getResultList();
            showMultiTableClass(countries, out);

            out.println("</body>\n"
                    + "</html>\n");
        }
    }

    private void showTableCountry(Collection<Country> countries, java.io.PrintWriter out) {

        out.println("<P ALIGN='center'><TABLE BORDER=1>");

        // table header
        out.println("<TR>");
        out.println("<TH>Country Name</TH>");
        out.println("<TH>Form of Government</TH>");
        out.println("<TH>Region</TH>");
        out.println("</TR>");

        Iterator<Country> iterator = countries.iterator();
        while (iterator.hasNext()) {
            Country country = iterator.next();
            out.println("<TR>");
            out.println("<TD>" + country.getName() + "</TD>");
            out.println("<TD>" + country.getGovernmentForm() + "</TD>");
            out.println("<TD>" + country.getRegion() + "</TD>");
            out.println("</TR>");
        }
        out.println("</TABLE></P>");
    }

    private void showUnTypedTable(Collection<Object[]> countries, java.io.PrintWriter out) {

        out.println("<P ALIGN='center'><TABLE BORDER=1>");

        // table header
        out.println("<TR>");
        out.println("<TH>Country Name</TH>");
        out.println("<TH>Region</TH>");
        out.println("<TH>Language</TH>");
        out.println("</TR>");

        Iterator<Object[]> iterator = countries.iterator();
        while (iterator.hasNext()) {
            Object[] country = iterator.next();
            out.println("<TR>");
            out.println("<TD>" + country[0] + "</TD>");
            out.println("<TD>" + country[1] + "</TD>");
            out.println("<TD>" + country[2] + "</TD>");
            out.println("</TR>");
        }
        out.println("</TABLE></P>");
    }

    private void showMultiTableClass(Collection<CountryRegionLanguageBean> countries, java.io.PrintWriter out) {

        out.println("<P ALIGN='center'><TABLE BORDER=1>");

        // table header
        out.println("<TR>");
        out.println("<TH>Country Name</TH>");
        out.println("<TH>Region</TH>");
        out.println("<TH>Language</TH>");
        out.println("</TR>");

        Iterator<CountryRegionLanguageBean> iterator = countries.iterator();
        while (iterator.hasNext()) {
            CountryRegionLanguageBean country = iterator.next();
            out.println("<TR>");
            out.println("<TD>" + country.getCountry() + "</TD>");
            out.println("<TD>" + country.getRegion() + "</TD>");
            out.println("<TD>" + country.getLanguage() + "</TD>");
            out.println("</TR>");
        }
        out.println("</TABLE></P>");
    }

}
