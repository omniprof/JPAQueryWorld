/*
 * Sample code for students written by Ken Fogel.
 * Creative Commons Licence
 * Freely use portions of this code in your solutions.
 * Provide attribution by referencing the git repository
 * that this code came from. 
 */
package com.kfwebstandard.jpaqueryworld.entities;

/**
 *
 * @author omni_
 */
public class CountryRegionLanguageBean {

    private String region;
    private String country;
    private String language;

    public CountryRegionLanguageBean(String country, String region, String language) {
        this.region = region;
        this.country = country;
        this.language = language;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

}
