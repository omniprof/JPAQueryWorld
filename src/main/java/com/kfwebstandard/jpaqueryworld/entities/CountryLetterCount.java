package com.kfwebstandard.jpaqueryworld.entities;

import java.util.Objects;

/**
 *
 * @author omni_
 */
public class CountryLetterCount {

    private String country;
    private long count;

    public CountryLetterCount(String country, long count) {
        this.country = country;
        this.count = count;
    }

    public CountryLetterCount() {
        this.country = "";
        this.count = 0;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.country);
        hash = 89 * hash + (int) (this.count ^ (this.count >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CountryLetterCount other = (CountryLetterCount) obj;
        if (this.count != other.count) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CountryLetterCount{" + "country=" + country + ", count=" + count + '}';
    }

}
